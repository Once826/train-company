import { Router } from 'express';
import * as db from '../db/index.js';

const router = Router();

router.get('/:trainId', async (req, res) => {
  try {
    const tid = req.params.trainId;
    const price = await db.getPrice(tid);
    if (price) {
      res.json(price);
    } else {
      res.status(404);
      res.json('train does not esist!');
    }
  } catch (error) {
    res.status(500);
    res.json(`Server hiba: ${error}`);
    console.log(error);
  }
});

router.delete('/:resId/delete', async (req, res) => {
  try {
    const { resId } = req.params;
    const rowsAff = await db.deleteReservation(resId);
    if (rowsAff.affectedRows === 1) {
      res.json({ message: 'Delete was succesfull!' });
    } else {
      res.json({ message: 'Delete was unsuccesfull!' });
    }
  } catch (error) {
    res.status(404);
    res.json(`delete error: ${error}`);
    console.log(error);
  }
});

router.delete('/:trainId/deletetrain', async (req, res) => {
  try {
    const { trainId } = req.params;
    await db.deleteResTrain(trainId);
    const rowsAff = await db.deleteTrain(trainId);
    if (rowsAff.affectedRows === 1) {
      res.json({ message: 'Delete was succesfull!' });
    } else {
      res.json({ message: 'Delete was unsuccesfull!' });
    }
  } catch (error) {
    res.status(404);
    res.json(`delete error: ${error}`);
    console.log(error);
  }
});

export default router;
