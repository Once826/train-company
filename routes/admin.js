import jwt from 'jsonwebtoken';
import { Router } from 'express';
import * as db from '../db/index.js';

const router = Router();

const secret = '1c28d07215544bd1b24faccad6c14a04';

router.get('/', async (request, response) => {
  try {
    const tok = request.headers.cookie;
    const items = await db.selectAllTrain();
    if (typeof tok !== 'undefined') {
      const token = tok.split('=');
      jwt.verify(token[1], secret, async (err, payload) => {
        const { user } = payload;
        const admin = await db.getRole(user);
        if (admin === 1) {
          response.render('admin', { user, admin, items });
        } else {
          response.redirect('home');
        }
      });
    } else {
      response.redirect('home');
    }
  } catch (error) {
    response.render('error', { error });
  }
});

router.post('/submit', async (request, response) => {
  try {
    await db.insertTrain(request.body);
    response.redirect('/');
  } catch (error) {
    response.render('error', { error });
  }
});

export default router;
