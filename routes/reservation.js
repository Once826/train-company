import { Router } from 'express';
import jwt from 'jsonwebtoken';
import * as db from '../db/index.js';

const router = Router();

const secret = '1c28d07215544bd1b24faccad6c14a04';

router.get('/', async (request, response) => {
  try {
    const tid = Number(request.query.trainId);
    const [items, reser] = await Promise.all([db.selectAllUser(), db.selectReservation(tid)]);
    const mess = String(request.query.message);
    const tok = request.headers.cookie;
    if (typeof tok !== 'undefined') {
      const token = tok.split('=');
      jwt.verify(token[1], secret, async (err, payload) => {
        const { user } = payload;
        const admin = await db.getRole(user);
        response.render('reservation', {
          items, reser, mess, tid, user, admin,
        });
      });
    } else {
      const user = 'You are not logged in';
      response.render('reservation', {
        items, reser, mess, tid, user, admin: 0,
      });
    }
  } catch (error) {
    response.render('error', { error });
  }
});

router.get('/submit', async (request, response) => {
  try {
    const userName = String(request.query.name);
    const userid = await db.getUserId(userName);
    const trainid = Number(request.query.id);
    const size = await db.getSize(trainid);
    if (size > 0) {
      await db.insertReservation(userid, trainid);
      const okque = `/reservation?message=ok&trainId=${trainid}`;
      response.redirect(okque);
    } else {
      const okque = `/reservation?message=full&trainId=${trainid}`;
      response.redirect(okque);
    }
  } catch (error) {
    const trainid = Number(request.query.id);
    const erque = `/reservation?message=error&trainId=${trainid}`;
    response.redirect(erque);
  }
});

export default router;
