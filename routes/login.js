import jwt from 'jsonwebtoken';
import { Router } from 'express';
import * as db from '../db/index.js';
import * as hash from '../utils/hash.js';

const router = Router();
const secret = '1c28d07215544bd1b24faccad6c14a04';

router.get('/', async (request, response) => {
  const register = String(request.query.register);
  const match = String(request.query.match);
  const tok = request.headers.cookie;
  jwt.verify(tok, secret, (error) => {
    if (error) {
      response.render('login', { register, match });
    } else {
      response.redirect('/');
    }
  });
});

router.post('/', async (request, response) => {
  try {
    const user = request.body.userName;
    const passwd = request.body.password;
    const password = await db.getPassword(user);
    const isAdmin = await db.getRole(user);
    const role = (isAdmin === 1) ? 'admin' : 'user';
    const match = await hash.checkHash(passwd, password);

    if (match) {
      const token = jwt.sign({ user, role }, secret);
      response.cookie('token', token, { httpOnly: true, sameSite: 'strict' });
      response.redirect('/');
    } else {
      const error = 'invalid username or password';
      response.status(401).render('error', { error });
    }
  } catch (error) {
    response.render('error', { error });
  }
});

router.post('/register', async (request, response) => {
  try {
    const user = request.body.ruserName;
    const passwd = request.body.rpassword;
    const passwdagain = request.body.rpasswordagain;

    if (passwd !== passwdagain) {
      const missmatch = '/login?match=fail';
      response.redirect(missmatch);
    }
    const hashed = await hash.createHash(passwd);
    await db.addUser(user, hashed, 0);

    const allGood = '/login?register=ok';
    response.redirect(allGood);
  } catch (error) {
    console.log(error);
    const failRegi = '/login?register=fail';
    response.redirect(failRegi);
  }
});

export default router;
