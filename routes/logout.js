import { Router } from 'express';

const router = Router();

router.get('/', async (request, response) => {
  response.clearCookie('token');
  response.clearCookie('role');
  response.redirect('login');
});

export default router;
