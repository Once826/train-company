import jwt from 'jsonwebtoken';
import { Router } from 'express';
import * as db from '../db/index.js';

const router = Router();
const secret = '1c28d07215544bd1b24faccad6c14a04';

router.get('/', async (request, response) => {
  try {
    const items = await db.selectAllTrain();
    const tok = request.headers.cookie;
    if (typeof tok !== 'undefined') {
      const token = tok.split('=');
      jwt.verify(token[1], secret, async (err, payload) => {
        const { user } = payload;
        const admin = await db.getRole(user);
        response.render('trains', { items, user, admin });
      });
    } else {
      const user = 'You are not logged in';
      response.render('trains', { items, user, admin: 0 });
    }
  } catch (error) {
    console.error(`Error ${error}`);
  }
});

router.get('/submit', async (request, response) => {
  try {
    const from = String(request.query.source);
    const to = String(request.query.destination);
    const minp = Number(request.query.minprice);
    const maxp = Number(request.query.maxprice);

    const items = await db.selectTrain(from, to, minp, maxp);
    const source = await db.selectFromTrain(from);
    // eslint-disable-next-line no-restricted-syntax
    for (const s of source) {
      // eslint-disable-next-line no-await-in-loop
      const dest = await db.selectToTrain(s.destination, to);
      // eslint-disable-next-line no-restricted-syntax
      for (const d of dest) {
        console.log(d.destination);
        const element = {
          trainId: s.trainId,
          source: (`${s.source} - ${s.destination}`),
          destination: d.destination,
          day: s.day,
          time: s.time,
          price: (s.price + d.price),
          type: s.type,
          size: s.size,
        };
        console.log(element);
        items.push(element);
      }
    }
    const tok = request.headers.cookie;
    if (typeof tok !== 'undefined') {
      const token = tok.split('=');
      jwt.verify(token[1], secret, async (err, payload) => {
        const { user } = payload;
        const admin = await db.getRole(user);
        response.render('trains', { items, user, admin });
      });
    } else {
      const user = 'You are not logged in';
      response.render('trains', { items, user, admin: 0 });
    }
  } catch (error) {
    response.render('error', { error });
  }
});

export default router;
