import jwt from 'jsonwebtoken';
import { Router } from 'express';
import * as db from '../db/index.js';

const router = Router();

const secret = '1c28d07215544bd1b24faccad6c14a04';

router.get('/', async (request, response) => {
  try {
    const tok = request.headers.cookie;
    if (typeof tok !== 'undefined') {
      const token = tok.split('=');
      jwt.verify(token[1], secret, async (err, payload) => {
        const { user } = payload;
        const admin = await db.getRole(user);
        response.render('home', { user, admin });
      });
    } else {
      const user = 'You are not logged in';
      response.render('home', { user, admin: 0 });
    }
  } catch (error) {
    console.error(`Error ${error}`);
  }
});

export default router;
