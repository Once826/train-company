import { Router } from 'express';
import jwt from 'jsonwebtoken';
import * as db from '../db/index.js';

const router = Router();

const secret = '1c28d07215544bd1b24faccad6c14a04';

router.get('/', async (request, response) => {
  try {
    const [items, reser] = await Promise.all([db.selectAllUser(), db.selectAllReservation()]);
    const mess = String(request.query.message);
    const tok = request.headers.cookie;
    if (typeof tok !== 'undefined') {
      const token = tok.split('=');
      jwt.verify(token[1], secret, async (err, payload) => {
        const { user } = payload;
        const admin = await db.getRole(user);
        response.render('myreservations', {
          items, reser, mess, user, admin,
        });
      });
    } else {
      const user = 'You are not logged in';
      response.render('myreservations', {
        items, reser, mess, user, admin: 0,
      });
    }
  } catch (error) {
    response.render('error', { error });
  }
});

export default router;
