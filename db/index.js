import { createPool } from 'mysql2';
import { promisify } from 'util';

const pool = createPool({
  connectionLimit: 10,
  database: 'trains',
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'password1',
});

pool.query(`CREATE TABLE IF NOT EXISTS trains (
  trainId int auto_increment primary key,
  source varchar(30),
  destination varchar(30),
  day varchar(30),
  time varchar(30),
  price int,
  type varchar(30),
  size int
  );`, (error) => {
  if (error) {
    console.error(`Create table error: ${error}`);
    process.exit(1);
  } else {
    console.log('Trains table created!');
  }
});

pool.query(`CREATE TABLE IF NOT EXISTS users (
  userId int auto_increment primary key,
  userName varchar(30),
  password varchar(100),
  admin int(11) DEFAULT NULL
);`, (error) => {
  if (error) {
    console.error(`Create table error: ${error}`);
    process.exit(1);
  } else {
    console.log('Users table created!');
  }
});

pool.query(`CREATE TABLE IF NOT EXISTS reservations (
  reservationId int auto_increment primary key,
  userId int,
  trainId int,
  FOREIGN KEY (userId) REFERENCES users(userId),
  FOREIGN KEY (trainId) REFERENCES trains(trainId)
);`, (error) => {
  if (error) {
    console.error(`Create table error: ${error}`);
    process.exit(1);
  } else {
    console.log('Reservations table created!');
  }
});

const db = promisify(pool.query).bind(pool);

export function insertTrain(t) {
  const queryStr = 'INSERT INTO trains (source, destination, day, time, price, type, size) VALUES (?, ?, ?, ?, ?, ?, ?);';
  return db(queryStr, [t.source, t.destination, t.day, t.time, t.price, t.type, t.size]);
}

export function selectAllTrain() {
  const queryStr = 'SELECT * FROM trains';
  return db(queryStr);
}

export function selectTrain(from, to, minp, maxp) {
  const queryStr = 'SELECT * FROM trains WHERE source = ? AND destination = ? AND price >= ? AND price <= ?';
  return db(queryStr, [from, to, minp, maxp]);
}

export function selectFromTrain(from) {
  const queryStr = 'SELECT * FROM trains WHERE source = ?';
  return db(queryStr, [from]);
}

export function selectToTrain(from, to) {
  const queryStr = 'SELECT * FROM trains WHERE source = ? AND destination = ?';
  return db(queryStr, [from, to]);
}

export function insertUser(u) {
  const queryStr = 'INSERT INTO users (userName) VALUES (?);';
  return db(queryStr, [u]);
}

export function selectAllUser() {
  const queryStr = 'SELECT * FROM users';
  return db(queryStr);
}

export function selectUser(name) {
  const queryStr = 'SELECT userId from users WHERE userName = ?';
  return db(queryStr, [name]);
}

export function selectAllTrainId() {
  const queryStr = 'SELECT trainId FROM trains';
  return db(queryStr);
}

export function getSize(tid) {
  const queryStr = 'SELECT size FROM trains WHERE trainId = ?';
  return db(queryStr, [tid]);
}

export function insertReservation(uid, tid) {
  const queryStr = 'INSERT INTO reservations (userId, trainId) VALUES (?, ?); UPDATE trains SET size = size - 1 WHERE trainId = ?';
  return db(queryStr, [uid, tid, tid]);
}

export function selectAllReservation() {
  const queryStr = 'SELECT users.userName, reservations.reservationId, trains.source, trains.destination, trains.day, trains.time FROM reservations, users, trains WHERE users.userId = reservations.userId AND reservations.trainId = trains.trainId';
  return db(queryStr);
}

export function selectReservation(tid) {
  const queryStr = 'SELECT users.userName, reservations.reservationId, trains.source, trains.destination, trains.day, trains.time FROM reservations, users, trains WHERE users.userId = reservations.userId AND reservations.trainId = trains.trainId AND reservations.trainId = ?';
  return db(queryStr, [tid]);
}

export function getPrice(id) {
  const queryStr = 'SELECT price FROM trains WHERE trainId = ?';
  return db(queryStr, [id]).then((res) =>  res[0].price);
}

export function deleteReservation(rid) {
  const queryStr = 'DELETE FROM reservations WHERE reservationId = ?';
  return db(queryStr, [rid]);
}

export function deleteTrain(tid) {
  const queryStr = 'DELETE FROM trains WHERE trainId = ?;';
  return db(queryStr, [tid, tid]);
}

export function deleteResTrain(tid) {
  const queryStr = 'DELETE FROM reservations WHERE trainId = ?';
  return db(queryStr, [tid]);
}

export function getPassword(userName) {
  const queryStr = 'SELECT password FROM users WHERE userName = ?';
  return db(queryStr, [userName]).then((res) => res[0].password);
}

export function getUserId(userName) {
  const queryStr = 'SELECT userId FROM users WHERE userName = ?';
  return db(queryStr, [userName]).then((res) => res[0].userId);
}

export function getRole(userName) {
  const queryStr = 'SELECT admin FROM users WHERE userName = ?';
  return db(queryStr, [userName]).then((res) => res[0].admin);
}

export function addUser(userName, password, admin) {
  console.log(userName);
  console.log(password);
  console.log(admin);
  const queryStr = 'INSERT INTO users (userName, password, admin) VALUES (?, ?, ?);';
  return db(queryStr, [userName, password, admin]);
}
