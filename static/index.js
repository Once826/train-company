// eslint-disable-next-line no-unused-vars
const getPrice = async (id) => {
  try {
    const res = await fetch(`/api/${id}`);
    try {
      if (!res.ok) {
        document.getElementById(`error${id}`).innerText = res.statusText;
      } else {
        document.getElementById(`price${id}`).hidden = false;
      }
    } catch (error) {
      document.getElementById(`price${id}`).innerText = error;
    }
  } catch (error) {
    document.getElementById(`price${id}`).innerText = error;
  }
};

// eslint-disable-next-line no-unused-vars
const deleteRes = async (id) => {
  try {
    const res = await fetch(`api/${id}/delete`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
    });

    if (!res) {
      document.getElementById(`delete${id}`).innerText = res.statusText;
    } else {
      try {
        const jsonOb = await res.json();
        document.getElementById(`reservation${id}`).innerText = jsonOb.message;
        document.getElementById(`del${id}`).hidden = true;
      } catch (error) {
        document.getElementById(`reservation${id}`).innerText = error;
      }
    }
  } catch (error) {
    document.getElementById(`reservation${id}`).innerText = error;
  }
};

// eslint-disable-next-line no-unused-vars
const delTrain = async (id) => {
  try {
    const res = await fetch(`api/${id}/deletetrain`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
    });

    if (!res) {
      document.getElementById('delresp').innerText = res.statusText;
    } else {
      try {
        const jsonOb = await res.json();
        document.getElementById('delresp').innerText = jsonOb.message;
        document.getElementById(`del${id}`).hidden = true;
      } catch (error) {
        document.getElementById('delresp').innerText = error;
      }
    }
  } catch (error) {
    document.getElementById('delresp').innerText = error;
  }
};
