import { join } from 'path';
import express from 'express';
import requestRoutes from './routes/index.js';
import homeRoutes from './routes/home.js';
import myreservationsRoutes from './routes/myreservations.js';
import apiRoutes from './routes/trains.js';
import loginRoutes from './routes/login.js';
import logoutRoutes from './routes/logout.js';
import adminRoutes from './routes/admin.js';
import reservationRoutes from './routes/reservation.js';
import authorize from './middleware/authorize.js';

const app = express();
const PORT = process.env.PORT || 3000;
const staticDir = join(__dirname, 'static');
app.use(express.static(join(__dirname, 'public')));
app.use(express.static(staticDir));
app.use(express.urlencoded({ extended: true }));

app.set('view engine', 'ejs');
app.set('views', join(process.cwd(), 'views'));

app.use('/', authorize(), requestRoutes);
app.use('/home', homeRoutes);
app.use('/myreservations', authorize(), myreservationsRoutes);
app.use('/login', loginRoutes);
app.use('/logout', authorize(), logoutRoutes);
app.use('/admin', authorize('admin'), adminRoutes);
app.use('/reservation', authorize(), reservationRoutes);
app.use('/api', apiRoutes);

app.listen(PORT, () => {
  console.log(`Server listening on http://localhost:${PORT} ...`);
});
