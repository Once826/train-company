import jwt from 'jsonwebtoken';

const secret = '1c28d07215544bd1b24faccad6c14a04';

function authorize(roles = ['admin', 'user']) {
  return (request, response, next) => {
    const tok = request.headers.cookie;
    if (typeof tok !== 'undefined') {
      const token = tok.split('=');

      jwt.verify(token[1], secret, (error, payload) => {
        if (error) {
          response.status(401);
          response.render('error', { error });
          return;
        }
        if (!roles.includes(payload.role)) {
          response.status(401);
          response.render('error', { error: 'You are not admin' });
        }
      });
      next();
    } else {
      if (roles.length === 5 && roles.includes('admin')) {
        response.status(401);
        response.render('error', { error: 'You are not admin' });
        return;
      }
      next();
    }
  };
}

export default authorize;
